package building_framework.page;

import org.openqa.selenium.WebDriver;

import building_framework.constants.*;

public class GoogleCloudMainPage extends GoogleCloudServicePage {

    public GoogleCloudMainPage(WebDriver driver) {
        super(driver);
    }

    public GoogleSearchResultsPage openCloudService() {
        driver.get(Constants.GOOGLE_CLOUD_SERVICE_URL);
        return new GoogleSearchResultsPage(driver);
    }
}

package building_framework.page;

import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.apache.logging.log4j.Logger;

import java.util.List;

import building_framework.constants.*;
import building_framework.util.*;

public class GoogleSearchResultsPage extends GoogleCloudServicePage {

    private final Logger logger = LogManager.getRootLogger();

    @FindBy(xpath = "//*[@class='mb2a7b']")
    private WebElement searchIcon;

    @FindBy(xpath = "//input[@class='mb2a7b']")
    private WebElement searchInputField;

    @FindBy(xpath = "//a[@class=\'gs-title\'and@href]")
    private List<WebElement> searchResults;

    public GoogleSearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public GoogleSearchResultsPage enableSearch() {
        searchIcon.click();
        searchInputField.sendKeys(Constants.SEARCH_REQUEST, Keys.ENTER);
        logger.info("Search was perfomed for request: " + Constants.SEARCH_REQUEST);
        return this;
    }

    public GooglePricingCalculatorPage chooseSearchResult() {
        GoogleCloudPageUtils.clickOnListValue(searchResults, Constants.SEARCH_REQUEST);
        return new GooglePricingCalculatorPage(driver);
    }


}

package building_framework.page;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import building_framework.constants.*;
import building_framework.util.*;

public class GooglePricingCalculatorPage extends GoogleCloudServicePage {

    private double calculatedTotalEstimate;
    private final Logger logger = LogManager.getRootLogger();

    public double getCalculatedTotalEstimate() {
        return calculatedTotalEstimate;
    }

    @FindBy(xpath = "//button[@class='devsite-snackbar-action']")
    private List<WebElement> cookiesOkBtn;

    @FindBy(xpath = "//article[@id='cloud-site']//iframe")
    private WebElement frameLocator;

    @FindBy(id = "input_99")
    private WebElement computerEngineElement;

    @FindBy(xpath = "//md-select[@id='select_124']")
    private WebElement seriesDropdownLocator;

    @FindBy(id = "select_126")
    private WebElement machineTypeDropdownLocator;

    @FindBy(id = "select_506")
    private WebElement gpuTypeDropdownLocator;

    @FindBy(id = "select_508")
    private WebElement numberOfGpuDropdownLocator;

    @FindBy(xpath = "//md-select[@id='select_465']")
    private WebElement localSsdDropdownLocator;

    @FindBy(xpath = "//md-select[@id='select_132']")
    private WebElement datacenterLocationDropdownLocator;

    @FindBy(xpath = "//md-select[@id='select_139']")
    private WebElement commitedUsageDropdownLocator;

    @FindBy(xpath = "//button[@class='md-raised md-primary cpc-button md-button md-ink-ripple']")
    private WebElement addToEstimateBtn;

    @FindBy(xpath = "//b[@class='ng-binding']")
    private WebElement calculatedPrice;

    @FindBy(id = "Email Estimate")
    private WebElement sendEmailEstimateBtn;

    public GooglePricingCalculatorPage(WebDriver driver) {
        super(driver);
    }

    public GooglePricingCalculatorPage switchToIframe() {
        GoogleCloudPageUtils.closeCookiesPopupWindow(cookiesOkBtn);
        driver.switchTo().frame(frameLocator);
        driver.switchTo().frame("myFrame");
        return this;
    }

    public GooglePricingCalculatorPage setPricingCalculatorTab() {
        String tabsLocator = "//md-pagination-wrapper//span[@class='ng-binding']";
        List<WebElement> tabsValues = driver.findElements(By.xpath(tabsLocator));
        GoogleCloudPageUtils.clickOnListValue(tabsValues, Constants.CALCULATOR_TAB);
        logger.info(Constants.CALCULATOR_TAB + " tab of Pricing Calculator was selected.");
        return this;
    }

    public GooglePricingCalculatorPage setNumberOfInstances() {
        computerEngineElement.click();
        computerEngineElement.sendKeys(Constants.NUMBER_OF_INSTANCES);
        return this;
    }

    public GooglePricingCalculatorPage setSeries() {
        seriesDropdownLocator.click();
        String seriesContainerLocator = "//div[@id='select_container_125']//div[@class='md-text ng-binding']";
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, seriesContainerLocator);
        List<WebElement> seriesDropdownValue = driver.findElements(By.xpath(seriesContainerLocator));
        GoogleCloudPageUtils.clickOnListValue(seriesDropdownValue, Constants.SERIES);
        return this;
    }

    public GooglePricingCalculatorPage setMachineType() {
        machineTypeDropdownLocator.click();
        String machineTypeContainerLocator = "//div[@id='select_container_127']//div[@class='md-text'or@class='md-text ng-binding']";
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, machineTypeContainerLocator);
        List<WebElement> machineTypeDropdownValue = driver.findElements(By.xpath(machineTypeContainerLocator));
        GoogleCloudPageUtils.clickOnListValue(machineTypeDropdownValue, Constants.MACHINE_TYPE);
        return this;
    }

    public GooglePricingCalculatorPage addGpu() {
        String checkboxLocator = "//div[@class='md-label']";
        List<WebElement> addGpuCheckbox = driver.findElements(By.xpath(checkboxLocator));
        GoogleCloudPageUtils.clickOnListValue(addGpuCheckbox, Constants.GPU);
        return this;
    }

    public GooglePricingCalculatorPage setGpuType() {
        gpuTypeDropdownLocator.click();
        String gpuTypeContainerLocator = "//div[@id='select_container_507']//div[@class='md-text ng-binding']";
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, gpuTypeContainerLocator);
        List<WebElement> gpuTypeDropdownValue = driver.findElements(By.xpath(gpuTypeContainerLocator));
        GoogleCloudPageUtils.clickOnListValue(gpuTypeDropdownValue, Constants.GPU_TYPE);
        return this;
    }

    public GooglePricingCalculatorPage setNumberOfGpu() {
        numberOfGpuDropdownLocator.click();
        String numberOfGpuContainerLocator = "//div[@id='select_container_509']//div[@class='md-text ng-binding']";
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, numberOfGpuContainerLocator);
        List<WebElement> numberOfGpuDropdownValue = driver.findElements(By.xpath(numberOfGpuContainerLocator));
        GoogleCloudPageUtils.clickOnListValue(numberOfGpuDropdownValue, Constants.NUMBER_OF_GPUS);
        return this;
    }

    public GooglePricingCalculatorPage setLocalSsd() {
        localSsdDropdownLocator.click();
        String localSsdContainerLocator = "//div[@id='select_container_466']//div[@class='md-text ng-binding']";
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, localSsdContainerLocator);
        List<WebElement> localSsdDropdownValue = driver.findElements(By.xpath(localSsdContainerLocator));
        GoogleCloudPageUtils.clickOnListValue(localSsdDropdownValue, Constants.LOCAL_SSD);
        return this;
    }

    public GooglePricingCalculatorPage setDatacenterLocation() {
        datacenterLocationDropdownLocator.click();
        String datacenterLocationContainerLocator = "//div[@id='select_container_133']//div[@class='md-text ng-binding']";
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, datacenterLocationContainerLocator);
        List<WebElement> datacenterLocationDropdownValue = driver.findElements(By.xpath(datacenterLocationContainerLocator));
        GoogleCloudPageUtils.clickOnListValue(datacenterLocationDropdownValue, Constants.DATACENTER_LOCATION);
        return this;
    }

    public GooglePricingCalculatorPage setCommitedUsage() {
        commitedUsageDropdownLocator.click();
        String commitedUsageContainerLocator = "//div[@id='select_container_140']//div[@class='md-text']";
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, commitedUsageContainerLocator);
        List<WebElement> commitedUsageDropdownValue = driver.findElements(By.xpath(commitedUsageContainerLocator));
        GoogleCloudPageUtils.clickOnListValue(commitedUsageDropdownValue, Constants.COMMITED_USAGE);
        logger.info("Necessary values are set");
        return this;
    }

    public GooglePricingCalculatorPage addToEstimate() {
        addToEstimateBtn.click();
        return this;
    }

    public GooglePricingCalculatorPage checkPriceIsCalculated() {
        calculatedTotalEstimate = GoogleCloudPageUtils.parseMessageToDouble(calculatedPrice.getText());
        logger.info("Total Estimate is calculated");
        return this;
    }

    public EmailPage selectEmailEstimate() {
        sendEmailEstimateBtn.click();
        return new EmailPage(driver);
    }
}

package building_framework.test;

import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;

import building_framework.page.*;

public class GoogleCalculatorTest extends CommonConditions {

    @Test
    public void googleCloudService() {
        GooglePricingCalculatorPage googlePricingCalculatorPage = new GoogleCloudMainPage(driver)
                .openCloudService()
                .enableSearch()
                .chooseSearchResult();
        EmailPage emailPage = googlePricingCalculatorPage
                .switchToIframe()
                .setPricingCalculatorTab()
                .setNumberOfInstances()
                .setSeries()
                .setMachineType()
                .addGpu()
                .setGpuType()
                .setNumberOfGpu()
                .setLocalSsd()
                .setDatacenterLocation()
                .setCommitedUsage()
                .addToEstimate()
                .checkPriceIsCalculated()
                .selectEmailEstimate();
        double actual = emailPage
                .openNewTab()
                .openEmailGeneratingService()
                .generateRandomEmail()
                .copyGeneratedEmail()
                .enterGeneratedEmailToCalculator()
                .sendEmailWithTotalEstimate()
                .checkEmail()
                .getEmailedTotalEstimate();

        double expected = googlePricingCalculatorPage.getCalculatedTotalEstimate();

        assertThat(actual, equalTo(expected));
    }
}
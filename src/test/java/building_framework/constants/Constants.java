package building_framework.constants;

public class Constants {
    public static final String GOOGLE_CLOUD_SERVICE_URL = "https://cloud.google.com/";
    public static final String SEARCH_REQUEST = "Google Cloud Pricing Calculator";
    public static final String CALCULATOR_TAB = "COMPUTE ENGINE";
    public static final String NUMBER_OF_INSTANCES = "4";
    public static final String SERIES = "N1";
    public static final String MACHINE_TYPE = "n1-standard-8 (vCPUs: 8, RAM: 30GB)";
    public static final String GPU = "Add GPUs.";
    public static final String GPU_TYPE = "NVIDIA Tesla V100";
    public static final String NUMBER_OF_GPUS = "1";
    public static final String LOCAL_SSD = "2x375 GB";
    public static final String DATACENTER_LOCATION = "Frankfurt (europe-west3)";
    public static final String COMMITED_USAGE = "1 Year";
    public static final String EMAIL_GENERATING_SERVICE_URL = " https://yopmail.com/";
}

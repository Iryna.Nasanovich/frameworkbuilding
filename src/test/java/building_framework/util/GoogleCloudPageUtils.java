package building_framework.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

public class GoogleCloudPageUtils {

    public static WebElement waitForElementToBeClickable(WebDriver driver, String optionLocator) {
        return new WebDriverWait(driver, Duration.ofSeconds(10)).
                until(ExpectedConditions.elementToBeClickable(By.xpath(optionLocator)));
    }

    public static WebElement waitForElementToBeClickable(WebDriver driver, WebElement elementLocator) {
        return new WebDriverWait(driver, Duration.ofSeconds(10)).
                until(ExpectedConditions.elementToBeClickable(elementLocator));
    }

    public static void closeCookiesPopupWindow(List<WebElement> cookiesPopup) {
        if (cookiesPopup.size() > 0) {
            cookiesPopup.get(0).click();
        }
    }

    public static void clickOnListValue(List<WebElement> listValues, String value) {
        for (WebElement element : listValues) {
            if (element.getText().strip().equals(value)) {
                element.click();
                break;
            }
        }
    }

    public static double parseMessageToDouble(String str) {
        str = str.replaceAll("[^0-9.,]+", " ");
        str = str.replaceAll(",+", "");
        List<String> strings = Arrays.asList(str.trim().split(" "));
        return Double.parseDouble(strings.get(0));
    }

    public static String buildEmail(List<WebElement> localPart) {
        return localPart.get(0).getText() + "@" + localPart.get(1).getText();
    }

    public static String buildString(List<WebElement> list) {
        return list.get(0).getText() + list.get(1).getText();
    }
}
